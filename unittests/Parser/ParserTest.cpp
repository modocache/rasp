#include "rasp/AST/Expression.h"
#include "rasp/Lexer/TokenStream.h"
#include "rasp/Parser/Parser.h"
#include "gtest/gtest.h"
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using namespace rasp;
using namespace rasp::parser;

class TokenVector : public lexer::TokenStream {
  std::vector<lexer::Token> Tokens;

public:
  TokenVector(std::vector<lexer::Token> &Tokens) : Tokens(std::move(Tokens)) {}

  lexer::Token getToken() {
    assert(Tokens.size() > 0);
    auto T = Tokens[0];
    Tokens.erase(Tokens.begin());
    return T;
  }
};

static lexer::Token lparen(int StartColumn) {
  return lexer::Token(lexer::TokenKind::LeftParenthesis, "(", 0, StartColumn);
}

static lexer::Token rparen(int StartColumn) {
  return lexer::Token(lexer::TokenKind::RightParenthesis, ")", 0, StartColumn);
}

static lexer::Token plusSign(int StartColumn) {
  return lexer::Token(lexer::TokenKind::Plus, "+", 0, StartColumn);
}

static lexer::Token number(int Value, int StartColumn) {
  return lexer::Token(lexer::TokenKind::Number, std::to_string(Value), 0,
                      StartColumn);
}

static lexer::Token eof(int StartColumn) {
  return lexer::Token(lexer::TokenKind::EndOfFile, "", 0, StartColumn);
}

TEST(ParserTest, Test_SingleArithmeticExpression) {
  std::vector<lexer::Token> Tokens;
  Tokens.push_back(lparen(1));
  Tokens.push_back(plusSign(2));
  Tokens.push_back(number(1, 4));
  Tokens.push_back(number(1, 6));
  Tokens.push_back(rparen(7));
  Tokens.push_back(eof(8));
  TokenVector TV(Tokens);
  Parser P(TV);

  auto AST = P.parse();
  EXPECT_NE(AST, nullptr);

  std::ostringstream OutputStream;
  AST->dump(OutputStream);
  EXPECT_EQ(OutputStream.str(),
            "{" // clang-format off
              "\"expression_kind\":\"arithmetic\","
              "\"parentheses\":["
                "{"
                  "\"text\":\"(\","
                  "\"source_range\":{"
                    "\"start_line\":0,"
                    "\"end_line\":0,"
                    "\"start_column\":0,"
                    "\"end_column\":0"
                  "}"
                "},"
                "{"
                  "\"text\":\")\","
                  "\"source_range\":{"
                    "\"start_line\":0,"
                    "\"end_line\":0,"
                    "\"start_column\":6,"
                    "\"end_column\":6"
                  "}"
                "}"
              "],"
              "\"operands\":["
                "{"
                  "\"kind\":\"number\","
                  "\"token\":{"
                    "\"text\":\"1\","
                    "\"source_range\":{"
                      "\"start_line\":0,"
                      "\"end_line\":0,"
                      "\"start_column\":3,"
                      "\"end_column\":3"
                    "}"
                  "}"
                "},"
                "{"
                  "\"kind\":\"number\","
                  "\"token\":{"
                    "\"text\":\"1\","
                    "\"source_range\":{"
                      "\"start_line\":0,"
                      "\"end_line\":0,"
                      "\"start_column\":5,"
                      "\"end_column\":5"
                    "}"
                  "}"
                "}"
              "]"
            "}"); // clang-format on
}
