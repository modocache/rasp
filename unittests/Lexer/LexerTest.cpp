#include "rasp/Lexer/Lexer.h"
#include "gtest/gtest.h"
#include <iostream>

using namespace rasp;
using namespace rasp::lexer;

TEST(LexerTest, Test_dumpTokens_RecurseCenterInput) {
  std::string Input = "(first (list 1 (+ 2 3) 9))";
  std::istringstream InputStream(Input);
  Lexer L(InputStream);

  std::ostringstream OutputStream;
  L.dumpTokens(OutputStream);

  EXPECT_EQ(OutputStream.str(),
            "[" // clang-format off
              "{"
                "\"text\":\"(\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":2,"
                  "\"end_column\":2"
                "}"
              "},"
              "{"
                "\"text\":\"first\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":3,"
                  "\"end_column\":7"
                "}"
              "},"
              "{"
                "\"text\":\"(\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":9,"
                  "\"end_column\":9"
                "}"
              "},"
              "{"
                "\"text\":\"list\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":10,"
                  "\"end_column\":13"
                "}"
              "},"
              "{"
                "\"text\":\"1\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":15,"
                  "\"end_column\":15"
                "}"
              "},"
              "{"
                "\"text\":\"(\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":17,"
                  "\"end_column\":17"
                "}"
              "},"
              "{"
                "\"text\":\"+\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":18,"
                  "\"end_column\":18"
                "}"
              "},"
              "{"
                "\"text\":\"2\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":20,"
                  "\"end_column\":20"
                "}"
              "},"
              "{"
                "\"text\":\"3\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":22,"
                  "\"end_column\":22"
                "}"
              "},"
              "{"
                "\"text\":\")\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":23,"
                  "\"end_column\":23"
                "}"
              "},"
              "{"
                "\"text\":\"9\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":25,"
                  "\"end_column\":25"
                "}"
              "},"
              "{"
                "\"text\":\")\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":26,"
                  "\"end_column\":26"
                "}"
              "},"
              "{"
                "\"text\":\")\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":27,"
                  "\"end_column\":27"
                "}"
              "},"
              "{"
                "\"text\":\"EOF\","
                "\"source_range\":{"
                  "\"start_line\":1,"
                  "\"end_line\":1,"
                  "\"start_column\":28,"
                  "\"end_column\":28"
                "}"
              "}]"); // clang-format on
}
