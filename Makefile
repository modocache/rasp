SOURCE_DIR:=${shell dirname ${realpath ${lastword ${MAKEFILE_LIST}}}}
BUILD_DIR := ${SOURCE_DIR}/build
BUILD_NINJA_DIR := ${BUILD_DIR}/Ninja
BUILD_XCODE_DIR := ${BUILD_DIR}/Xcode
CMAKE := cmake
CTEST := ctest
GIT := git

# == Top-level convenience commands ==

all: submodule-init build test

build: configure-ninja build-ninja

test: test-ninja

dev: format build test

clean: clean-ninja

xcode: configure-xcode open-xcode

# == Git Submodules ==

submodule-init:
	${GIT} submodule update --init --recursive

# == Configure CMake ==

configure-ninja:
	mkdir -p ${BUILD_NINJA_DIR}
	${CMAKE} \
		-B${BUILD_NINJA_DIR} \
		-H${SOURCE_DIR} \
		-G Ninja

configure-xcode:
	mkdir -p ${BUILD_XCODE_DIR}
	${CMAKE} \
		-B${BUILD_XCODE_DIR} \
		-H${SOURCE_DIR} \
		-G Xcode

# == Build ==

build-ninja:
	${CMAKE} --build ${BUILD_NINJA_DIR}

build-xcode:
	${CMAKE} --build ${BUILD_XCODE_DIR}

# == Test ==

test-ninja:
	cd ${BUILD_NINJA_DIR} && ${CTEST}

# == Clean ==

clean-ninja:
	rm -rf ${BUILD_NINJA_DIR}

clean-xcode:
	rm -rf ${BUILD_XCODE_DIR}

# == Other ==

format:
ifdef CLANG_FORMAT
	${CLANG_FORMAT} -i \
		$(shell \
			find ${SOURCE_DIR} \( -name '*.h' -o -name '*.cpp' \) \
			-not -path '${SOURCE_DIR}/external/*' \
			-not -path '${BUILD_DIR}/*')
endif

open-xcode:
	open ${BUILD_XCODE_DIR}/Rasp.xcodeproj
