#include "rasp/Parser/Parser.h"
#include "rasp/AST/ArithmeticExpression.h"
#include "rasp/AST/ArithmeticOperator.h"
#include "rasp/AST/CallExpression.h"
#include "rasp/AST/Expression.h"
#include "rasp/AST/NumberExpression.h"
#include "rasp/Lexer/Lexer.h"
#include "rasp/Lexer/Token.h"
#include "rasp/Lexer/TokenKind.h"
#include <iostream>
#include <memory>

using namespace rasp;
using namespace parser;

#pragma mark - Private Methods

lexer::Token Parser::getNextToken() {
  return CurrentToken = TokenStream.getToken();
}

std::unique_ptr<ast::Expression> Parser::parseExpression() {
  switch (CurrentToken.getKind()) {
  case lexer::TokenKind::LeftParenthesis:
    return parseParentheticalExpression();
  case lexer::TokenKind::Number:
    return parseNumberExpression();
  default:
    std::cerr << "Error: Unexpected token:" << std::endl << "\t";
    CurrentToken.dump(std::cerr);
    std::cerr << std::endl;
    return nullptr;
  }
}

std::unique_ptr<ast::Expression> Parser::parseParentheticalExpression() {
  auto LeftParenthesis = CurrentToken;
  getNextToken(); // Consume '('.
  switch (CurrentToken.getKind()) {
  case lexer::TokenKind::Identifier:
    return parseCallExpression(LeftParenthesis);
  case lexer::TokenKind::Plus:
  case lexer::TokenKind::Minus:
  case lexer::TokenKind::Asterisk:
  case lexer::TokenKind::ForwardSlash:
  case lexer::TokenKind::Percent:
    return parseArithmeticExpression(LeftParenthesis);
  default:
    std::cerr << "Error: Unexpected token:" << std::endl << "\t";
    CurrentToken.dump(std::cerr);
    std::cerr << std::endl;
    return nullptr;
  }
}

std::unique_ptr<ast::CallExpression>
Parser::parseCallExpression(lexer::Token LeftParenthesis) {
  auto Identifier = CurrentToken;
  getNextToken(); // Consume identifier.

  std::vector<std::unique_ptr<ast::Expression>> Arguments;

  // I assume a function needs to take at least one expression
  // as an argument...?
  auto FirstArgument = parseExpression();
  if (!FirstArgument)
    return nullptr;
  else
    Arguments.push_back(std::move(FirstArgument));

  while (true) {
    if (CurrentToken.getKind() == lexer::TokenKind::RightParenthesis) {
      auto RightParenthesis = CurrentToken;
      getNextToken(); // Consume ')'.
      return std::make_unique<ast::CallExpression>(LeftParenthesis, Identifier,
                                                   Arguments, RightParenthesis);
    }
    auto NextArgument = parseExpression();
    if (NextArgument)
      Arguments.push_back(std::move(NextArgument));
    else
      return nullptr;
  }
}

std::unique_ptr<ast::ArithmeticExpression>
Parser::parseArithmeticExpression(lexer::Token LeftParenthesis) {
  auto Operator = CurrentToken;
  getNextToken(); // Consume operator.

  auto LHS = parseExpression();
  if (!LHS)
    return nullptr;
  auto RHS = parseExpression();
  if (!RHS)
    return nullptr;

  if (CurrentToken.getKind() == lexer::TokenKind::RightParenthesis) {
    auto RightParenthesis = CurrentToken;
    getNextToken(); // Consume ')'.
    return std::make_unique<ast::ArithmeticExpression>(
        LeftParenthesis, Operator, std::move(LHS), std::move(RHS),
        RightParenthesis);
  } else {
    std::cout << "Error! Exprected rparen, got " << CurrentToken.getText()
              << std::endl;
    return nullptr;
  }
}

std::unique_ptr<ast::NumberExpression> Parser::parseNumberExpression() {
  auto Number = CurrentToken;
  getNextToken(); // Consume number.
  return std::make_unique<ast::NumberExpression>(Number);
}

#pragma mark - Public Methods

std::unique_ptr<ast::Expression> Parser::parse() {
  auto Expression = parseExpression();
  if (!Expression)
    return nullptr;
  if (CurrentToken.getKind() != lexer::TokenKind::EndOfFile) {
    std::cout << "Error: Expected end of input." << std::endl;
    return nullptr;
  }
  return Expression;
}
