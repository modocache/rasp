#include "rasp/AST/ArithmeticExpression.h"
#include <ostream>

using namespace rasp;
using namespace rasp::ast;

void ArithmeticExpression::dump(std::ostream &OutputStream) const {
  OutputStream << "{"
               << "\"expression_kind\":\"arithmetic\","
               << "\"parentheses\":[";
  LeftParenthesis.dump(OutputStream);
  OutputStream << ",";
  RightParenthesis.dump(OutputStream);
  OutputStream << "],"
               << "\"operands\":[";
  LHS->dump(OutputStream);
  OutputStream << ",";
  RHS->dump(OutputStream);
  OutputStream << "]}";
}
