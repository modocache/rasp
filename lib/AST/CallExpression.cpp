#include "rasp/AST/CallExpression.h"
#include <ostream>

using namespace rasp;
using namespace rasp::ast;

void CallExpression::dump(std::ostream &OutputStream) const {
  OutputStream << "{"
               << "\"expression_kind\":\"call\","
               << "\"parentheses\":[";
  LeftParenthesis.dump(OutputStream);
  OutputStream << ",";
  RightParenthesis.dump(OutputStream);
  OutputStream << "],"
               << "\"identifier\":";
  Identifier.dump(OutputStream);
  OutputStream << ","
               << "\"arguments\":[";
  for (auto &Argument : Arguments) {
    Argument->dump(OutputStream);
    if (Argument != Arguments.back()) {
      OutputStream << ",";
    }
  }
  OutputStream << "]}";
}
