#include "rasp/AST/ArithmeticOperator.h"
#include <cassert>

using namespace rasp;
using namespace rasp::ast;

ArithmeticOperator ast::getOperator(const std::string &Text) {
  assert(Text.size() == 1);
  switch (Text[0]) {
  case '+':
    return ArithmeticOperator::Add;
  case '-':
    return ArithmeticOperator::Subtract;
  case '*':
    return ArithmeticOperator::Multiply;
  case '/':
    return ArithmeticOperator::Divide;
  case '%':
    return ArithmeticOperator::Modulo;
  default:
    assert(false && "Invalid operator");
  }
}
