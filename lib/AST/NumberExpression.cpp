#include "rasp/AST/NumberExpression.h"
#include <ostream>

using namespace rasp;
using namespace rasp::ast;

void NumberExpression::dump(std::ostream &OutputStream) const {
  OutputStream << "{"
               << "\"kind\":\"number\","
               << "\"token\":";
  Number.dump(OutputStream);
  OutputStream << "}";
}
