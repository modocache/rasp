#include "rasp/Lexer/Lexer.h"
#include <iostream>

using namespace rasp;
using namespace rasp::lexer;

#pragma mark - Private Methods

int Lexer::getNextCharacter() { return InputStream.get(); }

Token Lexer::consumeIdentifier() {
  std::string Text;
  ++ColumnNumber;
  Text = LastCharacter;
  while (isalnum(LastCharacter = getNextCharacter())) {
    ++ColumnNumber;
    Text += LastCharacter;
  }

  return Token(TokenKind::Identifier, Text, LineNumber, ColumnNumber);
}

Token Lexer::consumeNumber() {
  std::string Text;
  ++ColumnNumber;
  Text = LastCharacter;
  while (isdigit(LastCharacter = getNextCharacter())) {
    ++ColumnNumber;
    Text += LastCharacter;
  }

  return Token(TokenKind::Number, Text, LineNumber, ColumnNumber);
}

#pragma mark - Public Methods

Token Lexer::getToken() {
  while (isspace(LastCharacter)) {
    if (LastCharacter == '\n') {
      ++LineNumber;
      ColumnNumber = 1;
    } else {
      ++ColumnNumber;
    }
    LastCharacter = getNextCharacter();
  }

  // Skip comments.
  if (LastCharacter == ';') {
    while (LastCharacter != '\n')
      LastCharacter = getNextCharacter();
    return getToken();
  }

  if (isalpha(LastCharacter))
    return consumeIdentifier();
  else if (isdigit(LastCharacter))
    return consumeNumber();
  else if (LastCharacter == EOF)
    return Token(TokenKind::EndOfFile, std::string(1, LastCharacter),
                 LineNumber, ++ColumnNumber);

  int Character = LastCharacter;
  LastCharacter = getNextCharacter();
  if (Character == '+')
    return Token(TokenKind::Plus, std::string(1, Character), LineNumber,
                 ++ColumnNumber);
  else if (Character == '(')
    return Token(TokenKind::LeftParenthesis, std::string(1, Character),
                 LineNumber, ++ColumnNumber);
  else if (Character == ')')
    return Token(TokenKind::RightParenthesis, std::string(1, Character),
                 LineNumber, ++ColumnNumber);
  else
    return Token(TokenKind::Other, std::string(1, Character), LineNumber,
                 ++ColumnNumber);
}

void Lexer::dumpTokens(std::ostream &OutputStream) {
  OutputStream << "[";
  while (true) {
    Token T = getToken();
    T.dump(OutputStream);
    if (T.getKind() == lexer::TokenKind::EndOfFile) {
      OutputStream << "]";
      return;
    } else {
      OutputStream << ",";
    }
  }
}
