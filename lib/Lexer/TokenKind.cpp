#include "rasp/Lexer/TokenKind.h"

using namespace rasp;
using namespace rasp::lexer;

std::string lexer::getName(TokenKind Kind) {
  switch (Kind) {
  case TokenKind::EndOfFile:
    return "end of file";
  case TokenKind::Identifier:
    return "identifier";
  case TokenKind::Number:
    return "number";
  case TokenKind::Plus:
    return "plus sign";
  case TokenKind::Minus:
    return "minus sign";
  case TokenKind::Asterisk:
    return "asterisk";
  case TokenKind::ForwardSlash:
    return "forward slash";
  case TokenKind::Percent:
    return "percent sign";
  case TokenKind::LeftParenthesis:
    return "left parenthesis";
  case TokenKind::RightParenthesis:
    return "right parenthesis";
  case TokenKind::Other:
    return "unknown token";
  }
}
