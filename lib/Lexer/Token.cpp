#include "rasp/Lexer/Token.h"
#include "rasp/Lexer/TokenKind.h"

using namespace rasp;
using namespace rasp::lexer;

void Token::dump(std::ostream &OutputStream) const {
  auto T = (getKind() == TokenKind::EndOfFile ? "EOF" : getText());
  OutputStream << "{"
               << "\"text\":\"" << T << "\","
               << "\"source_range\":";
  getSourceRange().dump(OutputStream);
  OutputStream << "}";
}
