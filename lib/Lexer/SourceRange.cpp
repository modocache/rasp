#include "rasp/Lexer/SourceRange.h"
#include <ostream>
#include <string>

using namespace rasp;
using namespace rasp::lexer;

void SourceRange::dump(std::ostream &OutputStream) const {
  OutputStream << "{"
               << "\"start_line\":" << getStartLine() << ","
               << "\"end_line\":" << getEndLine() << ","
               << "\"start_column\":" << getStartColumn() << ","
               << "\"end_column\":" << getEndColumn() << "}";
}
