# Rasp, a simple Lisp parser

This is my submission for my RC interview. I chose the "Lisp Parser" question:

> Write code that takes some Lisp code and returns an abstract syntax tree.
  The AST should represent the structure of the code and the meaning of each
  token. For example, if your code is given `(first (list 1 (+ 2 3) 9))`, it
  could return a nested array like `["first", ["list", 1, ["+", 2, 3], 9]]`.
>
> During your interview, you will pair on writing an interpreter to run the AST.
  You can start by implementing a single built-in function (for example, `+`)
  and add more if you have time.

To build this project, make sure you have CMake installed (`brew install cmake`
works on macOS, `apt-get install cmake` works on Ubuntu), then run `make` from
within this project's root directory.

## Project Structure

The `rasp` executable is defined in `tools/driver/driver.cpp`.

The executable uses libraries defined in `lib`:

- `lib/Lexer` is capable of transforming an textual input stream into tokens.
- `lib/AST` is capable of representing a syntax tree for Lisp syntax.
- `lib/Parser` uses a lexer in order to construct an AST.

The libraries' headers are declared in `include/rasp`.

Unit tests use googltest and are located in `unittest`.

Integration tests run the `rasp` executable on various input files. They're
located in `test`.

## Some caveats

This is the most fun I've had writing code in a while! As a result, though,
the project became a little bigger than I think I anticipated. Hope that's OK!

Also, this is the first or second C++ program I've ever written from scratch,
so if you notice something weird about it, please point it out to me! I felt
like practicing using C++ because I need to become more familiar with it for
my work (and my RC project!).

