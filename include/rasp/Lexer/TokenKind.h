#ifndef RASP_LEXER_TOKENKIND_H
#define RASP_LEXER_TOKENKIND_H

#include <string>

namespace rasp {
namespace lexer {

enum class TokenKind {
  EndOfFile = -1,

  Identifier = -2,
  Number = -3,

  Plus = -4,
  Minus = -5,
  Asterisk = -6,
  ForwardSlash = -7,
  Percent = -8,

  LeftParenthesis = -9,
  RightParenthesis = -10,

  Other = -11,
};

std::string getName(TokenKind Kind);
}
}

#endif
