#ifndef RASP_LEXER_TOKEN_H
#define RASP_LEXER_TOKEN_H

#include "rasp/Lexer/SourceRange.h"
#include "rasp/Lexer/TokenKind.h"
#include <ostream>
#include <string>

namespace rasp {
namespace lexer {

class Token {
  TokenKind Kind;
  std::string Text;
  SourceRange Range;

public:
  Token(TokenKind Kind, std::string Text, int Line, int Column)
      : Kind(Kind), Text(Text), Range(Line, Column - Text.size(), Column - 1) {}

  TokenKind getKind() const { return this->Kind; }
  const std::string getText() const { return this->Text; }
  SourceRange getSourceRange() const { return this->Range; }

  void dump(std::ostream &OutputStream) const;
};
}
}

#endif
