#ifndef RASP_LEXER_TOKENSTREAM_H
#define RASP_LEXER_TOKENSTREAM_H

namespace rasp {
namespace lexer {

class Token;

class TokenStream {
public:
  virtual Token getToken() = 0;
};
}
}

#endif
