#ifndef RASP_LEXER_LEXER_H
#define RASP_LEXER_LEXER_H

#include "rasp/Lexer/Token.h"
#include "rasp/Lexer/TokenStream.h"
#include <iostream>

namespace rasp {
namespace lexer {

class Lexer : public TokenStream {
  int LastCharacter = ' ';
  std::istream &InputStream;

  int LineNumber = 1;
  int ColumnNumber = 1;

  int getNextCharacter();
  Token consumeIdentifier();
  Token consumeNumber();

public:
  Lexer(std::istream &InputStream) : InputStream(InputStream) {}

  Token getToken();

  void dumpTokens(std::ostream &OutputStream);
};
}
}

#endif
