#ifndef RASP_LEXER_SOURCERANGE_H
#define RASP_LEXER_SOURCERANGE_H

#include <ostream>

namespace rasp {
namespace lexer {

class SourceRange {
  int StartLine;
  int EndLine;
  int StartColumn;
  int EndColumn;

public:
  SourceRange(int Line, int StartColumn, int EndColumn)
      : StartLine(Line), EndLine(Line), StartColumn(StartColumn),
        EndColumn(EndColumn) {}

  int getStartLine() const { return this->StartLine; }
  int getEndLine() const { return this->EndLine; }
  int getStartColumn() const { return this->StartColumn; }
  int getEndColumn() const { return this->EndColumn; }

  void dump(std::ostream &OutputStream) const;
};
}
}

#endif
