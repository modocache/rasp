#ifndef RASP_PARSER_PARSER_H
#define RASP_PARSER_PARSER_H

#include "rasp/AST/ArithmeticExpression.h"
#include "rasp/AST/CallExpression.h"
#include "rasp/AST/Expression.h"
#include "rasp/AST/NumberExpression.h"
#include "rasp/Lexer/Token.h"
#include "rasp/Lexer/TokenStream.h"
#include <memory>

namespace rasp {
namespace parser {

class Parser {
  lexer::TokenStream &TokenStream;
  lexer::Token CurrentToken;

  lexer::Token getNextToken();
  std::unique_ptr<ast::Expression> parseExpression();
  std::unique_ptr<ast::Expression> parseParentheticalExpression();
  std::unique_ptr<ast::CallExpression>
  parseCallExpression(lexer::Token LeftParenthesis);
  std::unique_ptr<ast::ArithmeticExpression>
  parseArithmeticExpression(lexer::Token LeftParenthesis);
  std::unique_ptr<ast::NumberExpression> parseNumberExpression();

public:
  Parser(lexer::TokenStream &TS)
      : TokenStream(TS), CurrentToken(TS.getToken()) {}

  std::unique_ptr<ast::Expression> parse();
};
}
}

#endif
