#ifndef RASP_AST_CALLEXPRESSION_H
#define RASP_AST_CALLEXPRESSION_H

#include "rasp/AST/Expression.h"
#include "rasp/Lexer/Token.h"
#include <memory>
#include <vector>

namespace rasp {
namespace ast {

class CallExpression : public Expression {
  lexer::Token LeftParenthesis;
  lexer::Token Identifier;
  std::vector<std::unique_ptr<Expression>> Arguments;
  lexer::Token RightParenthesis;

public:
  CallExpression(lexer::Token LeftParenthesis, lexer::Token Identifier,
                 std::vector<std::unique_ptr<Expression>> &Arguments,
                 lexer::Token RightParenthesis)
      : LeftParenthesis(LeftParenthesis), Identifier(Identifier),
        Arguments(std::move(Arguments)), RightParenthesis(RightParenthesis) {}

  void dump(std::ostream &OutputStream) const;
};
}
}

#endif
