#ifndef RASP_AST_ARITHMETICEXPRESSION_H
#define RASP_AST_ARITHMETICEXPRESSION_H

#include "rasp/AST/Expression.h"
#include "rasp/Lexer/Token.h"
#include <memory>
#include <ostream>

namespace rasp {
namespace ast {

class ArithmeticExpression : public Expression {
  lexer::Token LeftParenthesis;
  lexer::Token Operator;
  std::unique_ptr<Expression> LHS;
  std::unique_ptr<Expression> RHS;
  lexer::Token RightParenthesis;

public:
  ArithmeticExpression(lexer::Token LeftParenthesis, lexer::Token Operator,
                       std::unique_ptr<Expression> LHS,
                       std::unique_ptr<Expression> RHS,
                       lexer::Token RightParenthesis)
      : LeftParenthesis(LeftParenthesis), Operator(Operator),
        LHS(std::move(LHS)), RHS(std::move(RHS)),
        RightParenthesis(RightParenthesis) {}

  void dump(std::ostream &OutputStream) const;
};
}
}

#endif
