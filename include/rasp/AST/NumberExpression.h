#ifndef RASP_AST_NUMBEREXPRESSION_H
#define RASP_AST_NUMBEREXPRESSION_H

#include "rasp/AST/Expression.h"
#include "rasp/Lexer/Token.h"

namespace rasp {
namespace ast {

class NumberExpression : public Expression {
  lexer::Token Number;

public:
  NumberExpression(lexer::Token Number) : Number(Number) {}

  void dump(std::ostream &OutputStream) const;
};
}
}

#endif
