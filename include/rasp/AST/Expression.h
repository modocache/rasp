#ifndef RASP_AST_EXPRESSION_H
#define RASP_AST_EXPRESSION_H

#include <ostream>

namespace rasp {
namespace ast {

class Expression {
public:
  virtual void dump(std::ostream &OutputStream) const = 0;
};
}
}

#endif
