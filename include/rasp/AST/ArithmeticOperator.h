#ifndef RASP_AST_ARITHMETICEXPRESSION_H
#define RASP_AST_ARITHMETICEXPRESSION_H

#include "rasp/AST/Expression.h"
#include <string>

namespace rasp {
namespace ast {

enum class ArithmeticOperator {
  Add,
  Subtract,
  Multiply,
  Divide,
  Modulo,
};

ArithmeticOperator getOperator(const std::string &Text);
}
}

#endif
