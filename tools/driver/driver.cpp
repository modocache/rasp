#include "rasp/AST/Expression.h"
#include "rasp/Lexer/Lexer.h"
#include "rasp/Parser/Parser.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

using namespace rasp;

bool claimArgument(std::vector<std::string> &Arguments,
                   const std::string &Argument) {
  std::vector<std::string>::iterator I =
      std::find(Arguments.begin(), Arguments.end(), Argument);
  if (I != Arguments.end()) {
    Arguments.erase(I);
    return true;
  }
  return false;
}

int main(int argc, char *argv[]) {
  std::vector<std::string> Arguments(argv + 1, argv + argc);
  bool DumpTokens = claimArgument(Arguments, "-dump-tokens");
  bool DumpAST = claimArgument(Arguments, "-dump-ast");

  std::ifstream InputFileStream;
  std::istream *InputStream;
  if (Arguments.size() == 0) {
    InputStream = &std::cin;
  } else {
    auto InputFileName = Arguments[0];
    InputFileStream.open(InputFileName, std::ifstream::in);
    if (InputFileStream.fail()) {
      std::cout << "Error: Could not open file \"" << InputFileName << "\"."
                << std::endl;
      return 1;
    }
    InputStream = &InputFileStream;
  }

  lexer::Lexer Lexer(*InputStream);
  if (DumpTokens) {
    Lexer.dumpTokens(std::cout);
    return 0;
  }

  parser::Parser Parser(Lexer);
  auto AST = Parser.parse();
  if (!AST) {
    std::cout << "Error: Could not parse input stream." << std::endl;
    return 1;
  }

  if (DumpAST) {
    AST->dump(std::cout);
    return 0;
  }

  return 0;
}
