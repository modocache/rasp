function(add_rasp_unittest test_name test_sources)
  add_executable(${test_name} ${test_sources})
  rasp_target_include_directories(${test_name})
  rasp_unittest_target_include_directories(${test_name})
  target_link_libraries(${test_name}
                        PRIVATE
                        gtest gtest_main ${ARGN})
  add_test(${test_name} ${test_name} COMMAND ${test_name})
endfunction()

function(rasp_unittest_target_include_directories target_name)
  target_include_directories(
    ${target_name}
    PRIVATE
      "${CMAKE_SOURCE_DIR}/external/googletest/googletest/include")
endfunction()
