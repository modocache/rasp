function(add_rasp_library library_name)
  set(_library_name "rasp${library_name}")

  if (XCODE)
    # To display header files when browsing the project
    # in Xcode, grab the .h files in the include directory
    # corresponding to the library name, and add them
    # to the add_library() call.
    string(REGEX MATCHALL "/[^/]+" _split_path ${CMAKE_CURRENT_SOURCE_DIR})
    list(GET _split_path -1 _directory)
    file(GLOB_RECURSE _headers
         "${PROJECT_SOURCE_DIR}/include/rasp${_directory}/*.h")

    set_source_files_properties(${_headers}
                                PROPERTIES
                                HEADER_FILE_ONLY true)
  endif()

  add_library(${_library_name} ${ARGN} ${_headers})
  add_rasp_analysis(${_library_name} ${ARGN})
  rasp_target_include_directories(${_library_name})
endfunction()

function(add_rasp_executable executable_name)
  add_executable(${executable_name} ${ARGN})
  add_rasp_analysis(${executable_name} ${ARGN})
  rasp_target_include_directories(${executable_name})
endfunction()

# Adds an object library target that is built with the Clang static analyzer.
# Analyzer warnings are output alongside the build output.
function(add_rasp_analysis target_name)
  set(analysis_target_name "${target_name}_analysis")
  add_library(${analysis_target_name} OBJECT ${ARGN})
  rasp_target_include_directories(${analysis_target_name})
  set_target_properties(${analysis_target_name}
                        PROPERTIES
                        COMPILE_OPTIONS "--analyze")
endfunction()

function(rasp_target_include_directories target_name)
  target_include_directories(${target_name}
                             PUBLIC
                             "${PROJECT_SOURCE_DIR}/include"
                             "${PROJECT_BINARY_DIR}/include"
                             "${CMAKE_CURRENT_BINARY_DIR}")
endfunction()
